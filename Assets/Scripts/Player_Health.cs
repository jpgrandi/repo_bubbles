﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player_Health : MonoBehaviour {
	public const int MaxHealth = 100;
	public int CurrHealth;
	// Use this for initialization
	void Start () {
		CurrHealth = MaxHealth;

	}
	
	// Update is called once per frame
	void Update () {
		if (CurrHealth <= 0) {
			Debug.Log ("player died");
			SceneManager.LoadScene (3);
		}
	}

	public void takeDamage(int dmg) {
		CurrHealth -= dmg;
	}
}
