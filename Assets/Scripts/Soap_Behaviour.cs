﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Soap_Behaviour : MonoBehaviour {
	public GameObject bubblePrefab;
	public Transform bubbleSpawn;

	void OnCollisionEnter(Collision collision) {
		var hp = collision.gameObject.GetComponent<Enemy_Health> ();
		if (hp != null) {
			hp.takeDamage (35);
		}
		Foam ();
		Destroy (gameObject, 0.5f);
	}


	void Foam () {
		var bubble = (GameObject)Instantiate (
			bubblePrefab,
			bubbleSpawn.position,
			bubbleSpawn.rotation);
		Destroy (bubble, 0.1f);
	}
}
