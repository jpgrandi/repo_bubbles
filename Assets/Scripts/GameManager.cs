﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {
	// reminder: use singleton and add all other variables
	public int score;
	// Use this for initialization
	void Start () {
		score = 0;
	}
	
	// Update is called once per frame
	void Update () {
		//Debug.Log (GameObject.Find ("Player").GetComponent<Jetpack_Controls> ().currFuel);
		//Debug.Log (GameObject.Find ("Player").GetComponent<Player_Health> ().CurrHealth);
		//Debug.Log (GameObject.Find ("Player").GetComponent<Shooting_Controls> ().currAmmo);

		if (score >= 2) {
			SceneManager.LoadScene (4);
		}
	}
}
