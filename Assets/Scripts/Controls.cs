﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controls : MonoBehaviour {
	public float speed = 50f;
	public float maxSpeed = 3;
	public float jump = 150f;
	public Rigidbody2D rb;
	private Animator anim;
	public bool canJump = true;

	void OnCollisionEnter2D(Collision2D coll) {
		canJump = true;
	}

	// Use this for initialization
	void Start () {
		float xMov = speed * Time.deltaTime;
		rb = GetComponent<Rigidbody2D> ();
		anim = GetComponent<Animator> ();
		
	}
	
	// Update is called once per frame
	void Update () {
		anim.SetFloat ("Speed", Mathf.Abs (Input.GetAxis ("Horizontal")));
		
		if (Input.GetKeyDown (KeyCode.Space) && (canJump == true)) {
			rb.AddForce (transform.up * jump);
			canJump = false;
		}

		float h = Input.GetAxis ("Horizontal");

		rb.AddForce ((Vector2.right * speed) * h);

		if (rb.velocity.x > maxSpeed) {
			rb.velocity = new Vector2 (maxSpeed, rb.velocity.y);
		}
		if (rb.velocity.x < -maxSpeed) {
			rb.velocity = new Vector2 (-maxSpeed, rb.velocity.y);
		}
	}
}