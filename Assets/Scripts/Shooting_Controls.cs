﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting_Controls : MonoBehaviour {
	public GameObject bulletPrefab;
	public Transform bulletSpawn;
	public int maxAmmo = 3;
	public int currAmmo;

	// Use this for initialization
	void Start () {
		currAmmo = maxAmmo;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (Input.GetMouseButtonDown (0) && currAmmo > 0) {
			ShootSoap ();
		} else if (Input.GetKeyDown (KeyCode.R)) {
			Reload ();
		}
	}

	void ShootSoap () {
		currAmmo--;
		var bullet = (GameObject)Instantiate (
			             bulletPrefab,
			             bulletSpawn.position,
			             bulletSpawn.rotation);
		bullet.GetComponent<Rigidbody> ().velocity = bullet.transform.forward * 40;
		Destroy (bullet, 3.0f);
	}

	void Reload () {
		currAmmo = maxAmmo;
	}
}
