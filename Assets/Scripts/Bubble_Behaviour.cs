﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bubble_Behaviour : MonoBehaviour {

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		transform.Translate (Random.Range (-0.01f, 0.01f), Random.Range (-0.01f, 0.01f), Random.Range (-0.01f, 0.01f));
		transform.localScale += new Vector3 (-0.001f, -0.001f, -0.001f);
		float rnd = Random.Range (-0.1f, 0.1f);
		transform.localScale += new Vector3 (rnd, rnd, rnd);
	}
	void OnCollisionEnter(Collision collision) {
		var hp = collision.gameObject.GetComponent<Enemy_Health> ();
		if (hp != null) {
			hp.takeDamage (2);
		}
	}

	void OnBecameInvisible() {
		Destroy (gameObject);
	}
}
