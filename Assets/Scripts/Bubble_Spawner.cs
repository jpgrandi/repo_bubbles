﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bubble_Spawner : MonoBehaviour {
	public GameObject bubblePrefab;
	public Transform bubbleSpawn;

	// Use this for initialization
	void Start () {
		
	}

	// Update is called once per frame
	void Update () {
		transform.Translate (Random.Range (-0.5f, 0.5f), 0, Random.Range(-0.5f, 0.5f));
		Shoot ();
	}

	void Shoot () {
		var bubble = (GameObject)Instantiate (
			bubblePrefab,
			bubbleSpawn.position,
			bubbleSpawn.rotation);
			Destroy (bubble, 6.0f);
	}
}
