﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Health : MonoBehaviour {
	public const int MaxHealth = 500;
	public int CurrHealth;
	public Transform deathSpawn;
	public GameObject bubblePrefab;
	public bool isCreated;

	// Use this for initialization
	void Start () {
		CurrHealth = MaxHealth;
		isCreated = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (CurrHealth <= 0) {
			Debug.Log ("dead");
			FoamAndDie ();
		}
	}
	void FoamAndDie () {
		if (!isCreated) {
			isCreated = true;
			var bubble = (GameObject)Instantiate (
				            bubblePrefab,
				            deathSpawn.position,
				            deathSpawn.rotation);
			Destroy (bubble, 6.0f);
			GameObject.Find ("GameManager").GetComponent <GameManager> ().score += 1;
			Destroy (gameObject);
		}
	}
	public void takeDamage(int dmg) {
		CurrHealth -= dmg;
	}
}
