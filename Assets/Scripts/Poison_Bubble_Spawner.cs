﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Poison_Bubble_Spawner : MonoBehaviour {
	public GameObject bubblePrefab;
	public Transform bubbleSpawn;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		Shoot ();
	}

	void Shoot () {
		var bubble = (GameObject)Instantiate (
			bubblePrefab,
			bubbleSpawn.position,
			bubbleSpawn.rotation);
		Destroy (bubble, 3.0f);
	}
}
