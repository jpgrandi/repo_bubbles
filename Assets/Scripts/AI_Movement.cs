﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_Movement : MonoBehaviour {

	UnityEngine.AI.NavMeshAgent nav;
	Transform target;

	// Use this for initialization
	void Awake () {
		target = GameObject.FindGameObjectWithTag ("Player").transform;
		nav = GetComponent <UnityEngine.AI.NavMeshAgent> ();

	}

	void Start () {

	}

	// Update is called once per frame
	void Update () {
		nav.SetDestination (target.position);
	}
}
