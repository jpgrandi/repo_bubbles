﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement_Controls : MonoBehaviour {
	public float movSpeedZ = 25.0f;
	public float movSpeedX = 15.0f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		float movementZ = movSpeedZ * Input.GetAxis ("Vertical");
		float movementX = movSpeedX * Input.GetAxis ("Horizontal");

		transform.Translate (movementZ * Time.deltaTime, 0, movementX * Time.deltaTime * -1);
	}
}
