﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu_Buttons : MonoBehaviour {

	public void StartLevel_01() {
		SceneManager.LoadScene (1);
	}

	public void StartLevel_02() {
		SceneManager.LoadScene (2);
	}

	public void QuitGame() {
		Application.Quit ();
		UnityEditor.EditorApplication.isPlaying = false;
	}

	public void BackToMenu () {
		SceneManager.LoadScene (0);
	}
}
