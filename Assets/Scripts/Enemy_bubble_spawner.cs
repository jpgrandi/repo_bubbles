﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_bubble_spawner : MonoBehaviour {
	public Transform bubbleSpawn;
	public GameObject bubblePrefab;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		Shoot ();
	}

	void Shoot () {
		var bubble = (GameObject)Instantiate (
			bubblePrefab,
			bubbleSpawn.position,
			bubbleSpawn.rotation);
		Destroy (bubble, 2.5f);
	}
}
