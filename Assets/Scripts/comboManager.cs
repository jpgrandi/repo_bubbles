﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Combo {
	public string[] buttons;
	private int currIndex = 0;
	public float TimeBetweenPresses = 0.8f;
	private float LastPress;

	public Combo(string[] b) {
		buttons = b;
	}
		
	public bool Check() {
		if (Time.time > LastPress + TimeBetweenPresses) currIndex = 0; {
			if (currIndex < buttons.Length) {
				if ((buttons[currIndex] == "down" && Input.GetKeyDown(KeyCode.DownArrow)) ||
					(buttons[currIndex] == "up" && Input.GetKeyDown(KeyCode.UpArrow)) ||
					(buttons[currIndex] == "left" && Input.GetKeyDown(KeyCode.LeftArrow)) ||
					(buttons[currIndex] == "right" && Input.GetKeyDown(KeyCode.RightArrow)) ||
					(buttons[currIndex] == "a" && Input.GetKeyDown(KeyCode.A)) ||
					(buttons[currIndex] == "b" && Input.GetKeyDown(KeyCode.B))) {
					LastPress = Time.time;
					currIndex++;
				}

				if (currIndex >= buttons.Length){
					currIndex = 0;
					return true;
				}
				else return false;
			}
		}
		return false;
	}
}
	
public class comboManager : MonoBehaviour {

	public Combo konamiCode;
	void Start () {
		konamiCode = new Combo(new string[] {"up", "up", "down", "down", "left", "right", "b", "a"});
	}
	
	// Update is called once per frame
	void Update () {
		if (konamiCode.Check())
		{
			GameObject.Find ("Player").GetComponent<Jetpack_Controls> ().currFuel = 10000;
			Debug.Log("konamicode");
		}
	}
}
