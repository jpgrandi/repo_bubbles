﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jetpack_Controls : MonoBehaviour {
	public GameObject bulletPrefab;
	public Transform bulletSpawn;
	public int maxFuel = 100;
	public int currFuel;
	// Use this for initialization
	void Start () {
		currFuel = maxFuel;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (Input.GetKey (KeyCode.Space) && currFuel > 0) {
			Shoot ();	
		}
	}

	void Shoot () {
		currFuel -= 1;
		if (currFuel <= 0) {
			currFuel = 0;
		}
		Rigidbody rb;
		var bullet = (GameObject)Instantiate (
			             bulletPrefab,
			             bulletSpawn.position,
			             bulletSpawn.rotation);
		rb = bullet.GetComponent<Rigidbody> ();
		rb.AddForce (0, 10, 0, ForceMode.Impulse);
		Destroy (bullet, 12.0f);
	}

	void OnCollisionEnter(Collision collision) {
		if (collision.gameObject.tag == "Static") {
			Refuel (maxFuel);
		}
	}

	void Refuel(int amount) {
		currFuel = amount;
	}
		
}
